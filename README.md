### Classroom API ###

1. clone the repository
    1. `cd <project dir>`
2. `composer install`
3. `cp .env .env.local`
4. update database credentials
5. Create db if need: `bin/console doctrine:database:create`
6. Create table: `bin/console doctrine:schema:create`
7. Run local server: `bin/console server:run`
8. Using any REST client test application:

Here is schema of REST paths:
```
GET    /classrooms              -> list of classrooms
GET    /classroom/{classroomId} -> get single classroom by id
POST   /classroom               -> create classroom
PUT    /classroom/{classroomId} -> update classroom
DELETE /classroom/{classroomId} -> delete classroom
```
Class room model have such structure:
```json
{
    "id": "integer",
    "title": "string",
    "created": "timestamp",
    "active": "bool"
}
```

Fields `title` and `active` are required for `create` and `update` actions.
