<?php

namespace App\Service\Exception;

use App\Model\DTO\ErrorDTO;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

final class ValidationFailedException extends \LogicException
{
	private $errors = [];

	public static function createFromConstraintViolationList(ConstraintViolationListInterface $violations): self
	{
		$exception = new self();
		foreach ($violations as $violation) {
			/** @var ConstraintViolationInterface $violation */
			$exception->errors[] = new ErrorDTO($violation->getPropertyPath() . ': ' .$violation->getMessage());
		}

		return $exception;
	}

	public static function createFromFormErrors(FormErrorIterator $formErrors): self
	{
		$exception = new self();
		foreach ($formErrors as $error) {
			$exception->errors[] = new ErrorDTO($error->getMessage());
		}

		return $exception;
	}

	/**
	 * @return ErrorDTO[]
	 */
	public function getErrors(): array
	{
		return $this->errors;
	}
}
