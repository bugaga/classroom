<?php

namespace App\Service;

use App\Entity\Classroom;
use App\Model\DTO\ClassroomDTO;
use App\Model\ValueObject\ClassroomId;
use App\Service\Exception\ValidationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class ClassroomManager
{
	/** @var ObjectManager */
	private $om;

	/** @var ValidatorInterface */
	private $validator;

	/**
	 * ClassroomManager constructor.
	 *
	 * @param ObjectManager      $om
	 * @param ValidatorInterface $validator
	 */
	public function __construct(ObjectManager $om, ValidatorInterface $validator)
	{
		$this->om        = $om;
		$this->validator = $validator;
	}

	public function create(ClassroomDTO $classroomData): void
	{
		$this->validateClassroom($classroomData);
		$classroom = new Classroom($classroomData);

		$this->om->persist($classroom);
		$this->om->flush();
	}

	public function update(ClassroomId $id, ClassroomDTO $classroomDTO): void
	{
		$classroom = $this->getEntity($id);
		$classroom->update($classroomDTO);

		$this->om->flush();
	}

	/**
	 * @return ClassroomDTO[]
	 */
	public function getAll(): array
	{
		$classrooms = $this->om->getRepository(Classroom::class)->findAll();

		return array_map(
			function (Classroom $classroom) {
				return $classroom->toDTO();
			},
			$classrooms
		);
	}

	public function getSingle(ClassroomId $id): ClassroomDTO
	{
		$classroom = $this->getEntity($id);

		return $classroom->toDTO();
	}

	public function delete(ClassroomId $id): void
	{
		$classroom = $this->getEntity($id);

		$this->om->remove($classroom);
		$this->om->flush();
	}

	private function getEntity(ClassroomId $id): Classroom
	{
		$classroom = $this->om->getRepository(Classroom::class)->find($id->getValue());
		if (empty($classroom)) {
			throw EntityNotFoundException::fromClassNameAndIdentifier(Classroom::class, ['id' => $id->getValue()]);
		}

		return $classroom;
	}

	private function validateClassroom(ClassroomDTO $classroomData): void
	{
		$result = $this->validator->validate(
			$classroomData->jsonSerialize(),
			new Assert\Collection([
				'fields'           => [
					'title'  => [new Assert\NotBlank(), new Assert\Length(['min' => 2])],
					'active' => [new Assert\Type(['type' => 'bool'])]
				],
				'allowExtraFields' => true
			])
		);

		if ($result->count()) {
			throw ValidationFailedException::createFromConstraintViolationList($result);
		}
	}
}
