<?php

namespace App\EventListener;

use App\Model\DTO\ErrorDTO;
use App\Model\ErrorResponse;
use App\Service\Exception\ValidationFailedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ExceptionListener
{
	public function onKernelException(ExceptionEvent $event)
	{
		$exception = $event->getException();

		$responseModel = new ErrorResponse();
		switch (true) {
			case $exception instanceof ValidationFailedException:
				foreach ($exception->getErrors() as $error) {
					$responseModel->addError($error);
				}
				break;

			default:
				$responseModel->addError(new ErrorDTO($exception->getMessage(), $exception->getCode()));
		}

		$response = $event->getResponse() ?: new JsonResponse();
		$response->setData($responseModel);

		$event->setResponse($response);
	}
}