<?php

namespace App\Entity;

use App\Model\DTO\ClassroomDTO;
use App\Model\ValueObject\ClassroomId;
use \DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClassroomRepository")
 */
class Classroom
{
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $title;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $created_at;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $active = false;

	public function __construct(ClassroomDTO $classroomData)
	{
		$this->setValuesFromDTO($classroomData);
		$this->created_at = new DateTimeImmutable();
	}

	public function update(ClassroomDTO $classroomData)
	{
		$this->setValuesFromDTO($classroomData);
	}

	public function toDTO(): ClassroomDTO
	{
		$dto = new ClassroomDTO();
		if ($this->id) {
			$dto->setId(new ClassroomId($this->id));
		}

		$dto->setTitle($this->title);
		$dto->setActive($this->active);
		$dto->setCreatedAt($this->created_at);

		return $dto;
	}

	private function setValuesFromDTO(ClassroomDTO $classroomData)
	{
		$this->title      = $classroomData->getTitle();
		$this->active     = $classroomData->getActive();
	}
}
