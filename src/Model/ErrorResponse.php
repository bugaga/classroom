<?php

namespace App\Model;

use App\Model\DTO\ErrorDTO;

class ErrorResponse extends AbstractResponseModel
{
	private $errors = [];

	public function addError(ErrorDTO $error): void
	{
		$this->errors[] = $error;
	}

	public function jsonSerialize()
	{
		return array_merge(
			parent::jsonSerialize(),
			['errors' => $this->errors]
		);
	}

	protected function getResponseStatus(): string
	{
		return AbstractResponseModel::RESPONSE_STATUS_FAILED;
	}
}