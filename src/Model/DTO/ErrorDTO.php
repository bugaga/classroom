<?php

namespace App\Model\DTO;

final class ErrorDTO implements \JsonSerializable
{
	private $code;

	private $message;

	public function __construct(string $message, int $code = 0)
	{
		$this->message = $message;
		$this->code    = $code;
	}

	public function jsonSerialize()
	{
		return [
			'code'    => $this->code,
			'message' => $this->message
		];
	}
}