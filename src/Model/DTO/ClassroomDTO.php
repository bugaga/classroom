<?php

namespace App\Model\DTO;

use App\Model\ValueObject\ClassroomId;
use \JsonSerializable;
use \DateTimeInterface;

final class ClassroomDTO implements JsonSerializable
{
	/** @var ClassroomId|null */
	private $id;

	/** @var string|null */
	private $title;

	/** @var DateTimeInterface|null */
	private $createdAt;

	/** @var bool|null */
	private $active;

	public function jsonSerialize()
	{
		return [
			'id'         => $this->id ? $this->id->getValue() : null,
			'title'      => $this->title,
			'created_at' => $this->createdAt ? $this->createdAt->getTimestamp() : null,
			'active'     => $this->active,
		];
	}

	/**
	 * @return ClassroomId|null
	 */
	public function getId(): ?ClassroomId
	{
		return $this->id;
	}

	/**
	 * @param ClassroomId $id
	 */
	public function setId(ClassroomId $id): void
	{
		$this->id = $id;
	}

	/**
	 * @return string|null
	 */
	public function getTitle(): ?string
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title): void
	{
		$this->title = $title;
	}

	/**
	 * @return DateTimeInterface|null
	 */
	public function getCreatedAt(): ?DateTimeInterface
	{
		return $this->createdAt;
	}

	/**
	 * @param DateTimeInterface $createdAt
	 */
	public function setCreatedAt(DateTimeInterface $createdAt): void
	{
		$this->createdAt = $createdAt;
	}

	/**
	 * @return bool|null
	 */
	public function getActive(): ?bool
	{
		return $this->active;
	}

	/**
	 * @param bool $active
	 */
	public function setActive(bool $active): void
	{
		$this->active = $active;
	}
}
