<?php

namespace App\Model;

use \JsonSerializable;

abstract class AbstractResponseModel implements JsonSerializable
{
	public const RESPONSE_STATUS_OK     = 'Ok';
	public const RESPONSE_STATUS_FAILED = 'Failed';

	public function jsonSerialize()
	{
		return [
			'status' => $this->getResponseStatus()
		];
	}

	abstract protected function getResponseStatus(): string;
}