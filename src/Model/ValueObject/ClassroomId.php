<?php

namespace App\Model\ValueObject;

use \UnexpectedValueException;

class ClassroomId
{
	private $value;

	public function __construct(int $value)
	{
		if ($value < 1) {
			throw new UnexpectedValueException('The ID should be positive.');
		}

		$this->value = $value;
	}

	public function getValue(): int
	{
		return $this->value;
	}
}
