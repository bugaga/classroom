<?php

namespace App\Model;

class SuccessResponse extends AbstractResponseModel
{
	private $data = [];

	public function setData($data): void
	{
		$this->data = $data;
	}

	public function jsonSerialize()
	{
		return array_merge(
			parent::jsonSerialize(),
			['data' => $this->data]
		);
	}

	protected function getResponseStatus(): string
	{
		return AbstractResponseModel::RESPONSE_STATUS_OK;
	}
}