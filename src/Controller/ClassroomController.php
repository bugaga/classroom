<?php

namespace App\Controller;

use App\Form\ClassroomType;
use App\Model\DTO\ClassroomDTO;
use App\Model\SuccessResponse;
use App\Model\ValueObject\ClassroomId;
use App\Service\ClassroomManager;
use App\Service\Exception\ValidationFailedException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Json;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class ClassroomController extends AbstractController
{
	/** @var ValidatorInterface */
	private $validator;

	/** @var ClassroomManager */
	private $classroomManager;

	/**
	 * ClassroomController constructor.
	 *
	 * @param ValidatorInterface $validator
	 * @param ClassroomManager   $classroomManager
	 */
	public function __construct(ValidatorInterface $validator, ClassroomManager $classroomManager)
	{
		$this->validator        = $validator;
		$this->classroomManager = $classroomManager;
	}

	/**
	 * @Route("/classrooms", name="classroom.list", methods={"GET"})
	 */
	public function list()
	{
		$classrooms = $this->classroomManager->getAll();

		$response = new SuccessResponse();
		$response->setData($classrooms);

		return $this->json($response);
	}

	/**
	 * @Route("/classroom/{id}", name="classroom.single", methods={"GET"}, requirements={"id":"\d+"})
	 */
	public function single(int $id)
	{
		$classroom = $this->classroomManager->getSingle(new ClassroomId($id));

		$response = new SuccessResponse();
		$response->setData($classroom);

		return $this->json($response);
	}

	/**
	 * @Route("/classroom", name="classroom.create", methods={"POST"})
	 */
	public function create(Request $request)
	{
		$this->validateRequest($request);

		$form = $this->buildForm($request);
		if ($form->isValid()) {
			$classroomData = $form->getData();
			$this->classroomManager->create($classroomData);

			return $this->json(new SuccessResponse());
		}

		throw ValidationFailedException::createFromFormErrors($form->getErrors());
	}

	/**
	 * @Route("/classroom/{id}", name="classroom.update", methods={"PUT"}, requirements={"id":"\d+"})
	 */
	public function update(Request $request, int $id)
	{
		$this->validateRequest($request);

		$form = $this->buildForm($request);
		if ($form->isValid()) {
			$classroomData = $form->getData();
			$this->classroomManager->update(new ClassroomId($id), $classroomData);

			return $this->json(new SuccessResponse());
		}

		throw ValidationFailedException::createFromFormErrors($form->getErrors());
	}

	/**
	 * @Route("/classroom/{id}", name="classroom.delete", methods={"DELETE"}, requirements={"id":"\d+"})
	 */
	public function delete(int $id)
	{
		$this->classroomManager->delete(new ClassroomId($id));

		return $this->json(new SuccessResponse());
	}

	private function validateRequest(Request $request): void
	{
		if (!$request->headers->contains('Content-type', 'application/json')) {
			throw new BadRequestHttpException('Content-type should be application/json.');
		}

		$content = $request->getContent();
		$result  = $this->validator->validate($content, new Json());
		if ($result->count()) {
			throw new BadRequestHttpException('Invalid JSON.');
		}
	}

	private function buildForm(Request $request, ClassroomDTO $classroomData = null)
	{
		if ($classroomData === null) {
			$classroomData = new ClassroomDTO();
		}

		$requestData = json_decode($request->getContent(), true);

		$form = $this->createForm(ClassroomType::class, $classroomData);
		$form->submit($requestData);

		return $form;
	}
}
